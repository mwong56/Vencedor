package com.mwong56.vencedor;

import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class MainGamePanel extends SurfaceView implements SurfaceHolder.Callback {
    private static final String TAG = MainGamePanel.class.getSimpleName(); //Useful for debugging
    public static int score;
    public static int screenWidth;
    public static int screenHeight;
    public static int scrollSpeed;

    public static int totalPixels;
    public static int currPixels;
    public static boolean inPowerUp;
    private MainThread thread;
    private DisplayMetrics metrics;
    private float gravityX;
    private Drill drillBit;
    private Interactable diamond;
    private Interactable potion;
    private Interactable oil;
    private Interactable powerUp;
    private DrillBackground drillBackground;
    private Background background;
    private String avgFps;
    private long powerUpTimer;
    private int saveScrollSpeed;

    public MainGamePanel(Context context) {
        super(context);
        getHolder().addCallback(this);
        getHolder().setFormat(PixelFormat.RGBA_8888);
        initScreenMetrics(context);
        initGameMetrics();

        BitmapOptions options = new BitmapOptions();
        drillBit = new Drill(getResources(), 0, context);
        drillBit.setX(convertSizeToDeviceDependent((screenWidth - drillBit.getBitmap().getWidth()) / 2, context));
        drillBackground = new DrillBackground(getResources(), drillBit.getX(), drillBit.getY());
        diamond = new Interactable(BitmapFactory.decodeResource(getResources(),R.drawable.diamond, options.getOptions()), screenWidth/2, screenHeight+convertSizeToDeviceDependent(30,context));
        potion = new Interactable(BitmapFactory.decodeResource(getResources(), R.drawable.potion, options.getOptions()), screenWidth/4, screenHeight+convertSizeToDeviceDependent(230,context));
        oil = new Interactable(BitmapFactory.decodeResource(getResources(), R.drawable.oil_1, options.getOptions()), 3*screenWidth/4, screenHeight+convertSizeToDeviceDependent(450,context));
        powerUp = new Interactable(BitmapFactory.decodeResource(getResources(), R.drawable.power_up, options.getOptions()), 
                3*screenWidth/4, screenHeight+convertSizeToDeviceDependent(130,context));

        background = new Background(getResources(), context);

        thread = new MainThread(getHolder(), this);
        setFocusable(true);
    }

    public void initScreenMetrics(Context context) {
        metrics = context.getResources().getDisplayMetrics();
        screenWidth = metrics.widthPixels;
        screenHeight = metrics.heightPixels;
    }

    public void initGameMetrics() {
        score = 0;
        scrollSpeed = 10;
        totalPixels = 0;
        currPixels = 0;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
            int height) {
        // TODO Auto-generated method stub
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        // TODO Auto-generated method stub
        if (thread.getState() == Thread.State.TERMINATED)
        {
            thread = new MainThread(getHolder(), this);
            thread.setRunning(true);
            thread.start();
        }
        else
        {
            thread.setRunning(true);
            thread.start();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // TODO Auto-generated method stub
        boolean retry = true;
        while (retry) {
            try {
                thread.join(); //trys to kill thread;
                retry = false;
            } catch (InterruptedException e) {
                Log.d(TAG, "something went wrong at killing thread in surfaceDestroyed()");
                retry = true;
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        background.draw(canvas);
        drillBit.draw(canvas);        
        diamond.draw(canvas);
        potion.draw(canvas);
        oil.draw(canvas);
        drillBackground.draw(canvas);
        powerUp.draw(canvas);

        displayFps(canvas, avgFps);
        displayScore(canvas, MainGamePanel.score);
    }

    private void displayFps(Canvas canvas, String fps) {
        if (canvas != null && fps != null) {
            float scale = getResources().getDisplayMetrics().density;
            Paint paint = new Paint();
            paint.setARGB(255, 255, 255, 255);
            paint.setTextSize(15.0f * scale + 0.5f);
            canvas.drawText(fps, screenWidth - convertSizeToDeviceDependent(150,getContext()), screenHeight - convertSizeToDeviceDependent(20,getContext()),
                    paint);
        }
    }

    private void displayScore(Canvas canvas, int score) {
        if (canvas != null) {
            float scale = getResources().getDisplayMetrics().density;
            Paint paint = new Paint();
            paint.setTextSize(15.0f * scale + 0.5f);
            paint.setARGB(255, 255, 255, 255);
            canvas.drawText("Score: " + score, 10, screenHeight - convertSizeToDeviceDependent(20,getContext()), paint);
        }
    }

    public void update() {
        if (MainGamePanel.inPowerUp == true && drillBit.getPowerupFinish() == true) {
            if (System.nanoTime() - powerUpTimer < 5000000000L) {
                MainGamePanel.scrollSpeed = convertSizeToDeviceDependent(50, getContext());
            } 
            else {
                MainGamePanel.scrollSpeed = saveScrollSpeed;
                Log.d(TAG, "scrollSpeed: " + MainGamePanel.scrollSpeed);
                MainGamePanel.inPowerUp = false;
                drillBit.setInPowerUp(false);
            }
        }
        
        drillBit.update(gravityX);
        drillBackground.setX(drillBit.getX() - convertSizeToDeviceDependent(25, getContext()));
        detectInteractableCollision(diamond, -3);
        detectInteractableCollision(potion, 2);
        detectInteractableCollision(oil, 5);
        detectPowerupCollision(powerUp);
    }

    private void detectInteractableCollision(Interactable inter, int increment)
    {
        if(Rect.intersects(drillBit.getRect(), inter.getRect()) && inter.getVisibility() == true)
        {
            MainGamePanel.score = MainGamePanel.score + increment;
            inter.setVisibility(false);
        }
    }

    private void detectPowerupCollision(Interactable inter)
    {
        if(Rect.intersects(drillBit.getRect(), inter.getRect()) && inter.getVisibility() == true)
        {
            inter.setVisibility(false);
            if (MainGamePanel.inPowerUp == false) {
                MainGamePanel.inPowerUp = true;
                saveScrollSpeed = MainGamePanel.scrollSpeed;
                powerUpTimer = System.nanoTime();
                drillBit.setInPowerUp(true);
                drillBit.setPowerupFinish(false);
            }
        }
    }

    public void setAccelerationX(float gravityX) {
        this.gravityX = gravityX;
    }

    public void setAvgFps(String avgFps) {
        this.avgFps = avgFps;
    }

    public MainThread getThread() {
        return thread;
    }

    public static int convertSizeToDeviceDependent(int value,Context mContext) {
        DisplayMetrics dm = new DisplayMetrics();
        ((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(dm);
        return ((dm.densityDpi * value) / 160);
    }
}

package com.mwong56.vencedor;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

public class Background {
	//private static final String TAG = Background.class.getSimpleName(); //Useful for debugging
	private Bitmap bitmap;
	private Context context;
	private int x; 
	private int y;
	private int farBackground = 0;

	public Background(Resources resource, Context context) {
        BitmapOptions options = new BitmapOptions();
	    Bitmap bitmap = BitmapFactory.decodeResource(resource, R.drawable.sample_background, options.getOptions());
		this.bitmap = updateBitmap(bitmap);
		this.context = context;
		this.x = 0;
		this.y = 0;
	}

	public Bitmap updateBitmap(Bitmap bm) {
		Bitmap resized = Bitmap.createScaledBitmap(bm, MainGamePanel.screenWidth, MainGamePanel.screenHeight, false);
		return resized;
	}

	public Bitmap getBitmap() {
		return bitmap;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void draw(Canvas canvas) {
		if (MainGamePanel.inPowerUp == false) {
			if (MainGamePanel.currPixels < 1500) {
				MainGamePanel.scrollSpeed = MainGamePanel.convertSizeToDeviceDependent(5, context);
			}
			else if (MainGamePanel.currPixels > 1500 && MainGamePanel.currPixels < 3000) {
				MainGamePanel.scrollSpeed = MainGamePanel.convertSizeToDeviceDependent(6, context);
			}
			else if (MainGamePanel.currPixels > 3000 && MainGamePanel.currPixels < 4500) {
				MainGamePanel.scrollSpeed = MainGamePanel.convertSizeToDeviceDependent(7, context);
			}
			else if (MainGamePanel.currPixels > 4500 && MainGamePanel.currPixels < 6000) {
				MainGamePanel.scrollSpeed = MainGamePanel.convertSizeToDeviceDependent(8, context);
			}
			else if (MainGamePanel.currPixels > 6000 && MainGamePanel.currPixels < 30000) {
				MainGamePanel.scrollSpeed = MainGamePanel.convertSizeToDeviceDependent(9, context);
			}
			else if (MainGamePanel.currPixels > 30000 && MainGamePanel.currPixels < 60000) {
				MainGamePanel.scrollSpeed = MainGamePanel.convertSizeToDeviceDependent(12, context);
			}
			else if (MainGamePanel.currPixels > 60000) {
				MainGamePanel.scrollSpeed = MainGamePanel.convertSizeToDeviceDependent(15, context);
			}
			MainGamePanel.currPixels += MainGamePanel.scrollSpeed;
		}
		farBackground -=  MainGamePanel.scrollSpeed;
		MainGamePanel.totalPixels += MainGamePanel.scrollSpeed;
		int newFarBackground = bitmap.getHeight() - (-farBackground);
		if ( newFarBackground <= 0) {
			farBackground = 0;
			canvas.drawBitmap(bitmap, 0, farBackground, null);
		}
		else {
			canvas.drawBitmap(bitmap, 0, farBackground, null);
			canvas.drawBitmap(bitmap, 0, newFarBackground, null);
		}
	}
}

package com.mwong56.vencedor;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.WindowManager;
import android.widget.TextView;

public class StatsActivity extends Activity {
    TextView highScoreView;
    TextView highPixelsView;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats_activity);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        highScoreView = (TextView) findViewById(R.id.highscore_view);
        highPixelsView = (TextView) findViewById(R.id.pixels_view);
        
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        int highScore = sp.getInt("highScore", -1);
        int highPixels = sp.getInt("highPixels", -1);
        Log.d("Statsactivity", "highscore: " + highScore);
        
        if (highScore != -1) {
            highScoreView.setText("" + highScore);
        }
        
        if (highPixels != -1) {
            highPixelsView.setText("" + highPixels);
        }
    }
}

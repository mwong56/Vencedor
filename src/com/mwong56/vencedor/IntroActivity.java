package com.mwong56.vencedor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;

public class IntroActivity extends Activity {
    private Button playButton;
    private Button statsButton;
    private Button storeButton;
    private Button optionsButton;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_intro);
        
        playButton = (Button) findViewById(R.id.play_button);
        statsButton = (Button) findViewById(R.id.stats_button);
        storeButton = (Button) findViewById(R.id.store_button);
        optionsButton = (Button) findViewById(R.id.options_button);
        
        playButton.setOnClickListener(new OnClickListener() {
            
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent i = new Intent(IntroActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        });
        
        statsButton.setOnClickListener(new OnClickListener() {
            
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent i = new Intent(IntroActivity.this, StatsActivity.class);
                startActivity(i);
            }
        });
    }
}

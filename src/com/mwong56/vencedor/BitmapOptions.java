package com.mwong56.vencedor;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class BitmapOptions {
    
    BitmapFactory.Options options;
    
    public BitmapOptions() {
        options = new BitmapFactory.Options();
        options.inDither = false;
        options.inPreferQualityOverSpeed = false;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
    }
    
    public BitmapFactory.Options getOptions() {
        return this.options;
    }

}

package com.mwong56.vencedor;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.WindowManager;

public class MainActivity extends Activity implements SensorEventListener {
    //private static final String TAG = MainActivity.class.getSimpleName(); //Useful for debugging
    private MainGamePanel gamePanel;
    private MainThread mainThread;
    private SensorManager sensorManager;
    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sp = PreferenceManager.getDefaultSharedPreferences(this);
        gamePanel = new MainGamePanel(this);
        mainThread = gamePanel.getThread();
        setContentView(gamePanel);
    }
    
    private void saveScore() {
        int highScore = sp.getInt("highScore", -1);
        int highPixels = sp.getInt("highPixels", -1);
        Editor editor = sp.edit();
        if (highScore < MainGamePanel.score) {
            editor.putInt("highScore", MainGamePanel.score);
        }
        if (highPixels < MainGamePanel.totalPixels) {
            editor.putInt("highPixels", MainGamePanel.totalPixels);
        }
        editor.commit();
    }

    @Override
    protected void onDestroy() {
        saveScore();
        sensorManager.unregisterListener(this);
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        saveScore();
        sensorManager.unregisterListener(this);
        mainThread.setRunning(false);
        finish();
        super.onPause();
    }

    @Override
    protected void onResume() {
        /*
         * Register sensors for possible orientation change. (Although
         * orientation change is blocked here)
         */
        sensorManager.registerListener(this,
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
        super.onResume();
    }

    @Override
    protected void onStop() {
        saveScore();
        sensorManager.unregisterListener(this);
        super.onStop();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        // TODO Auto-generated method stub
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            gamePanel.setAccelerationX(event.values[0]);
        }
    }
}

package com.mwong56.vencedor;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;


public class DrillBackground  {
    private Bitmap bitmap1;
    private Bitmap bitmap2;
    private Bitmap bitmap3;
    private Bitmap bitmap4;
    private Bitmap currentBitmap;
    private int x; 
    private int y;

    public DrillBackground(Resources resource, int x, int y) {
        BitmapOptions options = new BitmapOptions();
        this.bitmap1 = BitmapFactory.decodeResource(resource, R.drawable.dirt_1, options.getOptions());
        this.bitmap2 = BitmapFactory.decodeResource(resource, R.drawable.dirt_2, options.getOptions());
        this.bitmap3 = BitmapFactory.decodeResource(resource, R.drawable.dirt_3, options.getOptions());
        this.bitmap4 = BitmapFactory.decodeResource(resource, R.drawable.dirt_4, options.getOptions());
        currentBitmap = bitmap1;
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Bitmap getBitmap() {
        return currentBitmap;
    }

    public void updateCurrent() {
        if (currentBitmap == bitmap1) {
            currentBitmap = bitmap2;
        }
        else if (currentBitmap == bitmap2) {
            currentBitmap = bitmap3;
        }
        else if (currentBitmap == bitmap3) {
            currentBitmap = bitmap4;
        }
        else {
            currentBitmap = bitmap1;
        }
    }

    public void draw(Canvas canvas) {
        updateCurrent();
        canvas.drawBitmap(currentBitmap, x, y, null);
    }
}

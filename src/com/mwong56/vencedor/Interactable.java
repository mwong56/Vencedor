package com.mwong56.vencedor;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;


public class Interactable  {
    private Bitmap bitmap;
    private int x; 
    private int y;
    private int currentY = 0;
    private boolean visible = true;
    private Rect rect;

    public Interactable(Bitmap bitmap, int x, int y) {
        this.bitmap = bitmap;
        this.x = x;
        this.y = y;
        rect = new Rect();
        setRect();
    }

    public void setRect() {
        this.rect.set(this.x, this.currentY, this.x+this.bitmap.getWidth(), this.currentY+this.bitmap.getHeight());
    }

    public int getX() {
        return x;
    }

    public Rect getRect() {
        return rect;
    }

    public int getCurrentY() {
        return currentY;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
        this.currentY = y;
    }

    public void setVisibility(boolean bool) {
        this.visible = bool;
    }

    public boolean getVisibility() {
        return this.visible;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void draw(Canvas canvas) {
	    this.currentY -= MainGamePanel.scrollSpeed;
	    if ( this.currentY <= - (bitmap.getHeight())) {
	        this.currentY = this.y;
	        this.visible = true;
	        canvas.drawBitmap(bitmap, this.x, this.currentY, null);
	    }
	    else if (visible == true) {
	        canvas.drawBitmap(bitmap, this.x, this.currentY, null);
	    }
	    rect.set(this.x, this.currentY, this.x+this.bitmap.getWidth(), this.currentY+this.bitmap.getHeight());
	}
}

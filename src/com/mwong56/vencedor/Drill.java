package com.mwong56.vencedor;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;

public class Drill  {
    private Bitmap bitmap1;
    private Bitmap bitmap2;
    private Bitmap bitmap3;
    private Bitmap bitmap4;

    private Bitmap powerup1;
    private Bitmap powerup2;
    private Bitmap powerup3;
    private Bitmap powerup4;
    private Bitmap powerup5;
    private Bitmap powerup6;
    private Bitmap powerup7;
    private Bitmap powerup8;
    private Bitmap powerup9;
    private Bitmap powerup10;
    private Bitmap powerup11;
    private Bitmap powerup12;
    private Bitmap powerup13;
    private Bitmap powerup14;
    private Bitmap powerup15;
    private Bitmap powerup16;
    private Bitmap powerup17;
    private Bitmap powerup18;
    private Bitmap powerup19;
    private Bitmap powerup20;

    private Bitmap currentBitmap, powerBitmap;
    private float xVelocity = 0.0f;
    private boolean inPowerup;
    private boolean powerupFinish;
    private Rect r;
    private int x; 
    private int y;
    private Context context;

    public Drill(Resources resource, int x, Context context) {
        BitmapOptions options = new BitmapOptions();
        bitmap1 = BitmapFactory.decodeResource(resource, R.drawable.drill_1, options.getOptions());
        bitmap2 = BitmapFactory.decodeResource(resource, R.drawable.drill_2, options.getOptions());
        bitmap3 = BitmapFactory.decodeResource(resource, R.drawable.drill_3, options.getOptions());
        bitmap4 = BitmapFactory.decodeResource(resource, R.drawable.drill_4, options.getOptions());

        powerup1 = BitmapFactory.decodeResource(resource, R.drawable.powerup_1, options.getOptions());
        powerup2 = BitmapFactory.decodeResource(resource, R.drawable.powerup_2, options.getOptions());
        powerup3 = BitmapFactory.decodeResource(resource, R.drawable.powerup_3, options.getOptions());
        powerup4 = BitmapFactory.decodeResource(resource, R.drawable.powerup_4, options.getOptions());
        powerup5 = BitmapFactory.decodeResource(resource, R.drawable.powerup_5, options.getOptions());
        powerup6 = BitmapFactory.decodeResource(resource, R.drawable.powerup_6, options.getOptions());
        powerup7 = BitmapFactory.decodeResource(resource, R.drawable.powerup_7, options.getOptions());
        powerup8 = BitmapFactory.decodeResource(resource, R.drawable.powerup_8, options.getOptions());
        powerup9 = BitmapFactory.decodeResource(resource, R.drawable.powerup_9, options.getOptions());
        powerup10 = BitmapFactory.decodeResource(resource, R.drawable.powerup_10, options.getOptions());
        powerup11 = BitmapFactory.decodeResource(resource, R.drawable.powerup_11, options.getOptions());
        powerup12 = BitmapFactory.decodeResource(resource, R.drawable.powerup_12, options.getOptions());
        powerup13 = BitmapFactory.decodeResource(resource, R.drawable.powerup_13, options.getOptions());
        powerup14 = BitmapFactory.decodeResource(resource, R.drawable.powerup_14, options.getOptions());
        powerup15 = BitmapFactory.decodeResource(resource, R.drawable.powerup_15, options.getOptions());
        powerup16 = BitmapFactory.decodeResource(resource, R.drawable.powerup_16, options.getOptions());
        powerup17 = BitmapFactory.decodeResource(resource, R.drawable.powerup_17, options.getOptions());
        powerup18 = BitmapFactory.decodeResource(resource, R.drawable.powerup_18, options.getOptions());
        powerup19 = BitmapFactory.decodeResource(resource, R.drawable.powerup_19, options.getOptions());
        powerup20 = BitmapFactory.decodeResource(resource, R.drawable.powerup_20, options.getOptions());

        currentBitmap = bitmap1;
        this.x = x;
        this.context = context;
        y = 0;
        inPowerup = false;
        powerupFinish = true;

        r = new Rect();
        r.set(this.x, this.y, this.x+this.currentBitmap.getWidth(), this.y+this.currentBitmap.getHeight());
    }

    public int getX() {
        return x;
    }

    public Rect getRect() {
        return r;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Bitmap getBitmap() {
        return currentBitmap;
    }

    public void update(float acceleration) {
        float frameTime = 0.50f;
        float bound = MainGamePanel.screenWidth - currentBitmap.getWidth();

        xVelocity += acceleration * frameTime;
        float xS = (xVelocity/2) * frameTime;

        this.x -= MainGamePanel.convertSizeToDeviceDependent((int) xS, context); 
        if (this.x > bound) {
            xVelocity = 0.0f;
            this.x = (int) bound;

        } 
        else if (this.x < 0) {
            xVelocity = 0.0f;
            this.x = 0;
        }

        r.set(this.x, this.y, this.x+this.currentBitmap.getWidth(), this.y+this.currentBitmap.getHeight());
    }

    public void updateCurrent() {
        if (currentBitmap == bitmap1) {
            currentBitmap = bitmap2;
        }
        else if (currentBitmap == bitmap2) {
            currentBitmap = bitmap3;
        }
        else if (currentBitmap == bitmap3) {
            currentBitmap = bitmap4;
        }
        else {
            currentBitmap = bitmap1;
        }
    }

    public void setPowerupFinish(boolean status) {
        this.powerupFinish = status;
    }

    public boolean getPowerupFinish() {
        return this.powerupFinish;
    }

    public void setInPowerUp(boolean status) {
        this.inPowerup = status;
    }

    public void powerAnimation() {
        if (powerBitmap == powerup1) {
            powerBitmap = powerup2;
        }
        else if (powerBitmap == powerup2) {
            powerBitmap = powerup3;
        }
        else if (powerBitmap == powerup3) {
            powerBitmap = powerup4;
        }
        else if (powerBitmap == powerup4) {
            powerBitmap = powerup5;
        }
        else if (powerBitmap == powerup5) {
            powerBitmap = powerup6;
        }
        else if (powerBitmap == powerup6) {
            powerBitmap = powerup7;
        }
        else if (powerBitmap == powerup7) {
            powerBitmap = powerup8;
        }
        else if (powerBitmap == powerup8) {
            powerBitmap = powerup9;
        }
        else if (powerBitmap == powerup9) {
            powerBitmap = powerup10;
        }
        else if (powerBitmap == powerup10) {
            powerBitmap = powerup11;
        }
        else if (powerBitmap == powerup11) {
            powerBitmap = powerup12;
        }
        else if (powerBitmap == powerup12) {
            powerBitmap = powerup13;
        }
        else if (powerBitmap == powerup13) {
            powerBitmap = powerup14;
        }
        else if (powerBitmap == powerup14) {
            powerBitmap = powerup15;
        }
        else if (powerBitmap == powerup15) {
            powerBitmap = powerup16;
        }
        else if (powerBitmap == powerup16) {
            powerBitmap = powerup17;
        }
        else if (powerBitmap == powerup17) {
            powerBitmap = powerup18;
        }
        else if (powerBitmap == powerup18) {
            powerBitmap = powerup19;
        }
        else if (powerBitmap == powerup19) {
            powerBitmap = powerup20;
            powerupFinish = true;
        }
        else {
            powerBitmap = powerup1;
        }
    }

    public void draw(Canvas canvas) {        
        updateCurrent();
        canvas.drawBitmap(currentBitmap, x, y, null);
        if (powerupFinish == false) {
            powerAnimation();               
        } 
        if (inPowerup){
        	canvas.drawBitmap(powerBitmap, x, y, null);
        }
        	
        
    }
}
